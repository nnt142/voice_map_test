// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require popper
//= require bootstrap-sprockets
//= require city_names
//= require jquery.autocomplete.min

$(document).on('show.bs.collapse', '#navbar-classic', function(){
  $('.navbar-mobile').addClass('collapse-shown');
});

$(document).on('hide.bs.collapse', '#navbar-classic', function(){
  $('.navbar-mobile').removeClass('collapse-shown');
});

$(document).on('ready', function(){
  if (!navigator.geolocation) {
    console.log('Geolocation is not supported by your browser');
  } else {
    navigator.geolocation.getCurrentPosition(function(position) {
      Swal.fire('VoiceMap has been using your location. Do you want to continue allowing this?')
    }, function() {
      Swal.fire({
        text: 'Allow VoiceMap to access your location?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Allow',
        cancelButtonText: "Don't Allow"
      }).then((result) => {
        if (result.value) {

        }
      });
    });
  }

  $('#search-autocomplete').autocomplete({
    lookup: city_names,
    lookupLimit: 5,
    appendTo: $('#search-autocomplete-result'),
    onSelect: function (suggestion) {
      console.log('You selected:' + suggestion.value);
    },
    onSearchStart: function () {
      $('#search-autocomplete-result .autocomplete-suggestions').css({"position": "relative", "z-index": "unset"});
    }
  });

  $('#search-autocomplete1').autocomplete({
    lookup: city_names,
    lookupLimit: 5,
    onSelect: function (suggestion) {
      console.log('You selected:' + suggestion.value);
    }
  });

  $('#search-autocomplete2').autocomplete({
    lookup: city_names,
    lookupLimit: 5,
    onSelect: function (suggestion) {
      console.log('You selected:' + suggestion.value);
    }
  });
});

$(window).scroll(function() {
  if($(this).scrollTop() > 50)  /*height in pixels when the navbar becomes non opaque*/ 
  {
    $('.header-transparent').addClass('header-transparent-scroll');
  } else {
    $('.header-transparent').removeClass('header-transparent-scroll');
  }
});